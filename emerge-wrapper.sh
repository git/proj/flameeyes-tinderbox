#!/bin/bash
#
# Copyright © 2008-2010 Diego Elio Pettenò <flameeyes@gentoo.org>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE CONSORTIUM DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET SOFTWARE
# CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
# ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
# SOFTWARE.

echo > /var/log/emerge.log

source /etc/portage/make.tinderbox.private.conf

if [[ -n ${BTI_ACCOUNT} ]]; then
    dent_me() {
        echo "$@" | bti ${TINDERBOX_PROXY:+--proxy "${TINDERBOX_PROXY}"} --host "${BTI_HOST}" --account "${BTI_ACCOUNT}" --password "${BTI_PASSWORD}" --background
    }
else
    dent_me() { :; }
fi

# Don't tweet this away if we're running a second-tier try
if [[ -z "${USE}" ]]; then
    dent_me "$1 queued"
fi

emerge --nospinner --oneshot --deep --update --keep-going --selective=n "$1" < /dev/null

res=$?

echo -5 | etc-update

if [[ $res != 0 ]]; then
    if ! fgrep -q ">>> emerge" /var/log/emerge.log; then
	# Here it means that the merge was rejected; the common case
	# it's a cyclic dependency that Portage cannot break, which is
	# unfortunately common when enabling tests or doc, usually
	# with Ruby ebuilds, but no longer limited to them. To find a
	# way around this, we first check for a build with USE=-doc,
	# and then one with FEATURES=-test as well. This does mean
	# that we skip the tests on a doc circular dependency
	# unfortunately.
	if [[ -z "${USE}" ]]; then
	    USE=-doc $0 "$@"
	elif [[ -z "${FEATURES}" ]]; then
	    FEATURES=-test $0 "$@"
	else
	    # This only hits the second time, so we're safely assuming
	    # that the package will reject to be merged as it is, why,
	    # we'll have to check.
	    dent_me "$1 merge #rejected"
	fi
    fi
fi

echo > /var/log/emerge.log
